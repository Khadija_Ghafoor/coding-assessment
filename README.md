# Coding assessment

## Task
Please create a C# console app that imports a [csv data file](https://databasesecuritylogs.blob.core.windows.net/test-files/data.csv) that contains sample product sale transactions and outputs the following metrics to a txt file (results.txt):

- Total quantity sold and total revenue per product
- The most popular product based on quantity sold
- Total revenue for all products sold
- The month with the highest revenue

## Submission
- Fork this repository
- Push your code
- Make a pull request

## Notes
- The application shouldn't use external libraries
- All functions should be unit tested